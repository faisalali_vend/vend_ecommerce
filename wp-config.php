<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'vend_ecommerce' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6U4n>sKVZGfW2ij)Qzo%ra=/cZfHjV!8^373P7]gYzJTq?&.f_IY)j$IRdN-U}pS' );
define( 'SECURE_AUTH_KEY',  'l<2  KR~L0|ze~^]9=^8W(1-F*nBO>ctj:LGp*)gKb6w@g){QemQ,Z2(k&/EvtZ ' );
define( 'LOGGED_IN_KEY',    'HW+5n7>Yuv]7*Ad0D,~%D>g^&NN=E^WXp{`.>K|(ql2MbDd!IY]RmLxfsZZ![=t}' );
define( 'NONCE_KEY',        '?2rI-1i;iK%1@p!~Qz;sK+6#A##xk8fE.HC/rvF>] U0[P2dSXXX4s_/=@AO_ Rq' );
define( 'AUTH_SALT',        'V[PY3ZI G;L[~::(}=h3M9fG/00T#?Og)Av1/H:Xtm!-]MnX6Y&||lO+3AI5<R$)' );
define( 'SECURE_AUTH_SALT', 'A-M#M$)s_4C__kXG?Nc$pAMj=3XB7x?d)V~@sc59`<f?lY~T_;nq*&o|$Do|PM%)' );
define( 'LOGGED_IN_SALT',   'M0cS5 K1Chs*L5N1%737g?{p}$t_W;K/V8d=,Nu$iQ7;.1v%|A44Qd}fh+|]P,P.' );
define( 'NONCE_SALT',       '9/i(-}FtJ;|Pzgvm}Es%Nm[5S:?wo(.CZj|J?@>eUkO3PBD,;I$L9ebl4k`$=|>N' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
